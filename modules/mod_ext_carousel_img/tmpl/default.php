<?php

// no direct access
defined('_JEXEC') or die;
if ($ext_load_jquery == 2) {
	if($ext_jquery == 1) { ?>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/<?php echo $ext_jquery_ver; ?>/jquery.min.js" type="text/javascript"></script>
	<?php } ?>
	<script type = "text/javascript" src = "<?php echo JURI::root() ?>/modules/mod_ext_carousel_img/js/jquery.featureCarousel.min.js"></script>
	<script type = "text/javascript">jQuery.noConflict();</script>
<?php }
$document->addCustomTag('
<style type="text/css">
	.carousel-container {
	  width:'.$ext_width.'px;
	}
	#carousel {
	  height:'.$ext_height.'px;
	  width:'.$ext_width.'px;
	}
</style>');
?>
<script type="text/javascript">
      jQuery(document).ready(function() {
        var carousel = jQuery("#carousel").featureCarousel({
          // include options like this:
          // (use quotes only for string values, and no trailing comma after last option)
          // option: value,
          // option: value
		  largeFeatureWidth:<?php echo $ext_largefeaturewidth;?>,
		  largeFeatureHeight:<?php echo $ext_largefeatureheight;?>,
		  smallFeatureWidth:<?php echo $ext_smallfeaturewidth;?>,
		  smallFeatureHeight:<?php echo $ext_smallfeatureheight;?>,
		  carouselSpeed:<?php echo $ext_carouselspeed;?>,
		  autoPlay:<?php echo $ext_autoplay;?>,
		  pauseOnHover: <?php echo $ext_pauseonhover;?>,
		  stopOnHover: <?php echo $ext_stoponhover;?>,
		  trackerIndividual: <?php echo $ext_trackerindividual;?>,
		  trackerSummation: <?php echo $ext_trackersummation;?>
        });
	});
</script>

<div class="mod_ext_carousel_img <?php echo $moduleclass_sfx ?>" style="height:<?php echo $ext_height;?>px; width:<?php echo $ext_width;?>px;">	
	<div class="carousel-container">
		<div id="carousel">
			<?php	
			for($n=0;$n < count($img);$n++) {			
				if( $img[$n] != '') {
					echo '<div class="carousel-feature"><a href="'.$url[$n].'" target="'.$target[$n].'"><img class="carousel-image" src="'.$img[$n].'" alt="'.$alt[$n].'"/></a><div class="carousel-caption">'.$html[$n].'</div></div>';
				}
			}	
			?>
		</div>
		<?php if ( $ext_button == 1) { ?>
			<div id="carousel-left"><img src="<?php echo JURI::root() ?>/modules/mod_ext_carousel_img/images/arrow-left.png"/></div>
			<div id="carousel-right"><img src="<?php echo JURI::root() ?>/modules/mod_ext_carousel_img/images/arrow-right.png" /></div>
		<?php } ?>
	</div>
	<div style="clear:both;"></div>
</div>