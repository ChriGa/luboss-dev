<?php 

// no direct access
defined('_JEXEC') or die;
error_reporting(E_ALL & ~E_NOTICE);

	$document 			= JFactory::getDocument();
	$document->addStyleSheet(JURI::base() . 'modules/mod_ext_carousel_img/css/default.css');
	$moduleclass_sfx	= $params->get('moduleclass_sfx');
	$ext_jquery   		= $params->get('ext_jquery',1);
	$ext_jquery_ver		= $params->get('ext_jquery_ver');
	$ext_load_jquery	= $params->get('ext_load_jquery',1);
	$ext_width			= $params->get('ext_width',700);
	$ext_height			= $params->get('ext_height',300);
	$ext_button			= $params->get('ext_button',1);
	if ($ext_load_jquery == 0) {
		if ($ext_jquery == 1) {	
			$document->addScript('http://ajax.googleapis.com/ajax/libs/jquery/'.$ext_jquery_ver.'/jquery.min.js');
			}
		$document->addScript(JURI::base() . 'modules/mod_ext_carousel_img/js/jquery.featureCarousel.min.js');
		$document->addCustomTag('<script type = "text/javascript">jQuery.noConflict();</script>');
	}
	if ($ext_load_jquery == 1) { 
		if ($ext_jquery == 1) {
			$document->addCustomTag('<script type = "text/javascript" src = "http://ajax.googleapis.com/ajax/libs/jquery/'.$ext_jquery_ver.'/jquery.min.js"></script>'); 
			}
		$document->addCustomTag('<script type = "text/javascript" src = "'.JURI::root().'modules/mod_ext_carousel_img/js/jquery.featureCarousel.min.js"></script>');
		$document->addCustomTag('<script type = "text/javascript">jQuery.noConflict();</script>');	
	}
	$ext_largefeaturewidth	= $params->get('ext_largefeaturewidth',0);
	$ext_largefeatureheight	= $params->get('ext_largefeatureheight',0);
	$ext_smallfeaturewidth	= $params->get('ext_smallfeaturewidth',0);
	$ext_smallfeatureheight = $params->get('ext_smallfeatureheight',0);
	$ext_carouselspeed 		= $params->get('ext_carouselspeed',1000);
	$ext_autoplay			= $params->get('ext_autoplay',0);
	$ext_pauseonhover		= $params->get('ext_pauseonhover', 'true');
	$ext_stoponhover		= $params->get('ext_stoponhover', 'false');
	$ext_trackerindividual	= $params->get('ext_trackerindividual', 'false');
	$ext_trackersummation	= $params->get('ext_trackersummation', 'true');
	
//array	
$names = array('img', 'alt', 'url', 'target', 'html');
$max = 10;
foreach($names as $name) {
    ${$name} = array();
    for($i = 1; $i <= $max; ++$i)
        ${$name}[] = $params->get($name . $i);
}	
require JModuleHelper::getLayoutPath('mod_ext_carousel_img', $params->get('layout', 'default'));

?>