<?php
/**
 * @author   	www.089webdesign.de
 * @copyright   Copyright (C) 2015 www.089webdesign.de All rights reserved.
 * @URL 		http://www.089webdesign.de
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die;
//include system
include_once(JPATH_ROOT . "/templates/" . $this->template . '/lib/system.php');

//include template Functions CG
include_once(JPATH_ROOT . "/templates/" . $this->template . '/template_functions.php');


?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<?php 
	// including head
	include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/head.php');
	?>
<?php /*CG 089: hiermit können via custom-Modul Scripte und Verlinkungen geladen werden mittels template "plain"*/ ?>
	
	<?php if ($this->countModules('endhead')) { ?>
			<jdoc:include type="modules" name="endhead" style="no" />
	<?php } ?>

</head>

<body class="site <?php echo $option
	. ' view-' . $view
	. ($layout ? ' layout-' . $layout : ' no-layout')
	. ($task ? ' task-' . $task : ' no-task')
	. ($itemid ? ' itemid-' . $itemid : '')
	. $body_class;
?>">

	<!-- Body -->
	
		<div class="site_wrapper animsition">
			<?php			
			
			// including header
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/header.php');			
			
			// including header
			//include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/menu.php');			
			
			// including slider
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/slider.php');
			
			// including top
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/breadcrumbs.php');	
			
			// including top
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/top.php');	
			
			// including top2
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/top2.php');	
									
			// including content nur wenn nicht Startseite
			if ($currentMenuID != 101 ) {			
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/content.php');	
			}
			// including bottom
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom.php');	
			
			// including bottom2
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom2.php');
			
			// including content
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/footer.php');				
			
			?>					
			
		</div>
	
	
	<jdoc:include type="modules" name="debug" style="none" />
<script type="text/javascript">

	<?php // siehe: http://git.blivesta.com/animsition/ ?>
		jQuery(document).ready(function() {
		  
		  jQuery(".animsition").animsition({
		  
		    inClass               :   "fade-in",
		    outClass              :   "fade-out",
		    inDuration            :    1800,
		    outDuration           :    1000,
		    linkElement           :   '.animsition-link',
		    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
		    loading               :    true,
		    loadingParentElement  :   'body', //animsition wrapper element
		    loadingClass          :   'animsition-loading',
		    unSupportCss          : [ 'animation-duration',
		                              '-webkit-animation-duration',
		                              '-o-animation-duration'
		                            ],
		    <?php /*"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
		    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration". */ ?>
		    
		    overlay               :   false,
		    
		    overlayClass          :   'animsition-overlay-slide',
		    overlayParentElement  :   'body'
		  });
		});
</script>	
		<?php //nav_banner hover script ?>
		<?php // <script src="http://ajax.googleapis.com/ajax/libs/jquery.min.js" type="text/javascript"></script> ?>
		<script src="templates/luboss/js/jquery.hoverdir.js" type="text/javascript"></script>
		<script type="text/javascript">

			jQuery(function($) {
				$(' #da-thumbs > li ').each(function() {
					$(this).hoverdir();
				});
			});
		</script>

		<script type="text/javascript">jQuery.noConflict();</script>

	<?php if ($currentMenuID != 101 && $menueFixed != 0) { // Top Menue scroll down pos->fixed NUR wenn NICHT Startseite und wenn Parameter unter templates aus "Ja"

			print 	'<style type="text/css">
							.f-nav { 
								z-index: 9999; 
								width: 1280px;
								position: fixed; 
								background: #ecedef;
								display: block;
								margin: 0px auto;
								padding-bottom: 10px;
								font-weight: 400;
								opacity: 0.7;
								transition: opacity 0.4s ease-in-out;
							} 

							.f-nav:hover {
								opacity: 1;
							}

							.f-nav ul.nav {
								top: 0;
							}

					</style>
				<script src="templates/luboss/js/menue_fix.js" type="text/javascript"></script>';
		}
	?>

<?php 
	$owlAutoplay = 3000;
	$itemsDesktop = "[1920,4]";
	$home = false;
		
		if($currentMenuID == 101) { $home = true; $owlAutoplay = 2000; $itemsDesktop = "[1280,15]"; $rewindSpeed = "rewindSpeed: 8000,"; }

			if ($this->countModules('endhead')) { ?>
						<script type="text/javascript">
						<?php // siehe: http://owlgraphic.com/owlcarousel/demos/lazyLoad.html ?>
							jQuery(document).ready(function() {
							  jQuery("#owl-demo").owlCarousel( {
							    items : 15,
								lazyLoad : false,
								navigation : true,
								autoPlay: <?php print $owlAutoplay ?>, //Set AutoPlay to 3 seconds
								itemsDesktop : <?php print $itemsDesktop ?>,
								<?php if($home) print $rewindSpeed."\n"; ?> <?php // wg string ausgabe incl ";" muss hier mit \n benutzt werden ?>
								itemsDesktopSmall : <?php print ($home) ? "[979,10]" : "[979,6]"; ?>,
								itemsTablet : [736,4],
								itemsMobile : [479,2]
								});
								  // Custom Navigation Events
									  jQuery(".next").click(function(){
									    owl.trigger('owl.next');
									  })
									  jQuery(".prev").click(function(){
									    owl.trigger('owl.prev');
									  })				 
							});
						</script>
				<?php } ?>			

</body>
</html>