<?php
/**
 * @author   	ClearTemplates.com
 * @copyright   Copyright (C) 2015 ClearTemplates.com. All rights reserved.
 * @URL 		https://cleartemplates.com/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<footer class="clear-footer" role="contentinfo">
	<div class="container clear-footer-wrap">		
		
		<div class="row-fluid">
			
					
			<?php if ($this->countModules('footnav')) : ?>
			<div class="footnav">
				<div class="module_footer position_footnav">
					<jdoc:include type="modules" name="footnav" style="none" />
				</div>			
			</div>
			<?php endif ?>	
			
			<div class="span12 footer">
				<jdoc:include type="modules" name="footer" style="none" />
			</div>

			
			<div class="copyright span6">
				<p>&copy; <?php print date(Y);?> Juwelier Luboss Germering / München, Trauringe, Verlobungsringe, Schmuck und Uhren</p>
			</div>			
			
		</div>
		
	</div>
</footer>
			