<?php
/**
 * @author   	ClearTemplates.com
 * @copyright   Copyright (C) 2015 ClearTemplates.com. All rights reserved.
 * @URL 		https://cleartemplates.com/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');
$doc->addScript('templates/' . $this->template . '/js/template.js');
$doc->addScript('templates/' . $this->template . '/js/menu.js');
$doc->addScript('templates/' . $this->template . '/js/modernizr.custom.97074.js');
$doc->addScript('templates/' . $this->template . '/js/jquery.hoverdir.js');
$doc->addScript('templates/' . $this->template . '/js/jquery.animsition.min.js');

// Add Stylesheets
$doc->addStyleSheet('templates/' . $this->template . '/css/template.css');
$doc->addStyleSheet('templates/' . $this->template . '/css/overrides.css');
$doc->addStyleSheet('templates/' . $this->template . '/css/responsive.css');
$doc->addStyleSheet('templates/' . $this->template . '/css/animsition.min.css');

// Load optional RTL Bootstrap CSS
JHtml::_('bootstrap.loadCss', false, $this->direction);

?>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jdoc:include type="head" />
	<?php // Use of Google Font ?>

		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700,600,600italic' rel='stylesheet' type='text/css'>


	
	<!--[if lt IE 9]>
		<script src="<?php echo $this->baseurl; ?>/media/jui/js/html5.js"></script>
	<![endif]-->