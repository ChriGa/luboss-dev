<?php
/**
 * @author   	ClearTemplates.com
 * @copyright   Copyright (C) 2015 ClearTemplates.com. All rights reserved.
 * @URL 		https://cleartemplates.com/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die;
?>

<header class="header">
	<div class="header-wrapper">
      <!-- Wrap the .navbar in .container to center it within the absolutely positioned parent. -->
      <div class="container ">
		<div class="row-fluid">

           
			<div class="header-inner clearfix">
				<div class="span5 logo pull-left visible-desktop">
					<a class="brand" href="<?php echo $this->baseurl; ?>">
						<?php echo $logo; ?>
						<?php if ($this->params->get('sitedescription')) : ?>
							<?php echo '<div class="site-description">' . htmlspecialchars($this->params->get('sitedescription')) . '</div>'; ?>
						<?php endif; ?>
					</a>
				</div>
				<div class="clear-phone-logo">
					<a class=" hidden-desktop " href="<?php echo $this->baseurl; ?>">
							<?php echo $logo; ?>
							<?php if ($this->params->get('sitedescription')) : ?>
								<?php echo '<div class="site-description">' . htmlspecialchars($this->params->get('sitedescription')) . '</div>'; ?>
							<?php endif; ?>
					</a>
				</div>
				
			<?php if ($this->countModules('menu')) { ?>
				<div class="span7 navbar pull-right">
					<div class="navbar-inner">
			<?php } ?>

					<!-- Responsive Navbar Part 1: Button for triggering responsive navbar (not covered in tutorial). Include responsive CSS to utilize. -->
					<button type="button" class="btn btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
					  <span class="icon-bar"></span>
					  <span class="icon-bar"></span>
					  <span class="icon-bar"></span>
					</button>           

				<!-- Responsive Navbar Part 2: Place all navbar contents you want collapsed withing .navbar-collapse.collapse. -->
					             
					  <?php if ($this->countModules('menu')) : ?>
						<div class="nav-collapse collapse "  role="navigation">
							<jdoc:include type="modules" name="menu" style="" />
						</div>
						<?php endif; ?>
					  
					<!--/.nav-collapse -->
					</div><!-- /.navbar-inner -->
				</div><!-- /.navbar -->

			<?php // } ?>
			</div>       
        
		</div>
      </div> <!-- /.container -->
    </div>
</header>