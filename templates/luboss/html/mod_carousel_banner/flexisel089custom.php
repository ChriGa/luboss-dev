<?php
/**
 * @version		$Id: mod_carousel_banner.php 2.0
 * @Joomla 1.7  by Rony S Y Zebua
 * @Official site http://www.templateplazza.com
 * @package		Joomla 1.7.x
 * @subpackage	mod_minifrontpage
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

require_once JPATH_ROOT . '/components/com_banners/helpers/banner.php';

$count				= (int) $params->get('count', 2);
$image_width 		= (int) $params->get('width');
$image_height 		= (int) $params->get('height');
$thumb_width 		= (int) $params->get('thumb_width', 73 );
$thumb_height 		= (int) $params->get('thumb_height', 42 );
$thumb_option		= $params->get('thumb_option', 'exact');

$baseurl        = ''.JURI::base();
$modulebase		= ''.JURI::base().'modules/mod_carousel_banner/';

?>

	<ul id="mycarousel<?php echo '-'.$module->id; ?>">
		
		<?php 
		
		foreach($list as $item) 
		{ 
			$link = JRoute::_('index.php?option=com_banners&task=click&id='. $item->id);
			$imageurl = $item->params->get('imageurl');
			?>

			<li>

			<?php
			if (BannerHelper::isImage($imageurl)) 
			{
					// Image based banner
					$alt = $item->params->get('alt');
					$alt = $alt ? $alt : $item->name;
					$alt = $alt ? $alt : JText::_('MOD_SUPERCAROUSEL');
					?>
					<img
						src="<?php echo $baseurl . $imageurl; ?>"
						alt="<?php echo $alt;?>" title="<?=htmlspecialchars($item->name, ENT_QUOTES, 'UTF-8'); ?>" 
					/>	
			</li>
										
			<?php 
			} 
		
		} /*end foreach */ 
		?>
			
	</ul><!-- end wrapper -->